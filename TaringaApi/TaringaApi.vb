﻿Imports System.Net
Imports System.Text
Imports System.IO
Imports System.Runtime.InteropServices

Public Class TaringaApi
    <DllImport("urlmon.dll", CharSet:=CharSet.Auto)> _
    Private Shared Function FindMimeFromData(ByVal pBC As IntPtr, <MarshalAs(UnmanagedType.LPWStr)> ByVal pwzUrl As String, <MarshalAs(UnmanagedType.LPArray, ArraySubType:=UnmanagedType.I1, SizeParamIndex:=3)> ByVal pBuffer As Byte(), ByVal cbSize As Integer, <MarshalAs(UnmanagedType.LPWStr)> ByVal pwzMimeProposed As String, ByVal dwMimeFlags As Integer, <MarshalAs(UnmanagedType.LPWStr)> ByRef ppwzMimeOut As String, ByVal dwReserved As Integer) As Integer
    End Function
    Public CookieJar As New CookieContainer
    ''' <summary>
    ''' Obtiene los Shouts seleccionados
    ''' </summary>
    ''' <param name="name">Tipo de shouts.</param>
    ''' <param name="filter">Si es pin, espeficar que hashtag.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetShouts(ByVal name As String, ByVal filter As String) As String

        Try
            Dim response As String = PostReq("http://www.taringa.net/ajax/feed/fetch", {{"feedName=", name}, {"&filterName=", filter}, {"&key=", Mi.user_key}})
            If response = "False" Then
                Return False
            Else
                Return response
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Console.Add(ex.Message)
        End Try


        Return False

    End Function
    Public Function login(ByVal Nick As String, ByVal pass As String) As String

        Try

            Dim response As String = PostReq("https://www.taringa.net/registro/login-submit.php", {{"nick=", Nick}, {"&pass=", pass}})

            If response = "False" Then
                Return False

            Else

                Return response

            End If

        Catch ex As Exception
            Form1.errorC = ex.Message
            Console.Add("Error: " & ex.Message)
            Return False

        End Try


        Return False

    End Function

    Public Function PostReq(ByVal url As String, ByVal params As String(,)) As String
        Dim request As HttpWebRequest
        Dim response As HttpWebResponse = Nothing
        Dim reader As StreamReader
        Dim address As Uri
        Dim data As StringBuilder
        Dim byteData() As Byte
        Dim postStream As Stream = Nothing
        Dim paramet As String

        For a As Integer = 0 To params.GetUpperBound(0)

            For b As Integer = 0 To params.GetUpperBound(1)

                paramet &= params(a, b)

            Next
        Next

        address = New Uri(url)

        ' Create the web request  
        request = DirectCast(WebRequest.Create(address), HttpWebRequest)

        ' Set type to POST
        request.Method = "POST"
        ' UAgent
        request.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36"
        ' Conttype
        request.ContentType = "application/x-www-form-urlencoded"
        ' Accept
        request.Accept = "text/html, */*; q=0.01"


        ' Create the data we want to send


        ' Create a byte array of the data we want to send  
        byteData = UTF8Encoding.UTF8.GetBytes(paramet)
        request.Headers.Add("Origin:http://taringa.net")
        ' request.Headers.Add("X-Requested-With:XMLHttpRequest")
        request.Referer = "http://www.taringa.net/mi"
        ' Set the content length in the request headers  
        request.ContentLength = byteData.Length
        request.CookieContainer = CookieJar
        ' Write data  
        Try
            postStream = request.GetRequestStream()
            postStream.Write(byteData, 0, byteData.Length)
        Finally
            If Not postStream Is Nothing Then postStream.Close()
        End Try

        Try
            ' Get response  
            response = DirectCast(request.GetResponse(), HttpWebResponse)

            ' Get the response stream into a reader  
            reader = New StreamReader(response.GetResponseStream())

            ' Console application output  

            Return reader.ReadToEnd()



        Finally
            If Not response Is Nothing Then response.Close()
        End Try


        Return False
    End Function

    Function UploadPic(ByVal LocalFile As String, ByVal url As String, ByVal varpar As String) As String
        Dim filepath As String = LocalFile
        'Dim url As String = "http://www.FAKESITE.com/php/Upload/index.php"
        Dim copo As String = "Content-Disposition: form-data; name=""" & varpar & """;"
        Dim filetype As String = GetMimeType(LocalFile)
        Dim boundary As String = IO.Path.GetRandomFileName
        Dim header As New System.Text.StringBuilder()
        header.AppendLine("--" & boundary)
        header.Append(copo)
        header.AppendFormat(" filename=""{0}""", IO.Path.GetFileName(filepath))
        header.AppendLine()
        header.AppendLine("Content-Type: " & filetype)
        header.AppendLine()

        Dim headerbytes() As Byte = System.Text.Encoding.UTF8.GetBytes(header.ToString)
        Dim endboundarybytes() As Byte = System.Text.Encoding.ASCII.GetBytes(vbNewLine & "--" & boundary & "--" & vbNewLine)

        Dim req As Net.HttpWebRequest = Net.HttpWebRequest.Create(url)
        req.Referer = "http://kn3.net/"
        req.Headers.Add("Origin: http://kn3.net")
        req.Expect = "100"
        req.ContentType = "multipart/form-data; boundary=" & boundary
        req.ContentLength = headerbytes.Length + New IO.FileInfo(filepath).Length + endboundarybytes.Length
        req.Method = "POST"

        Dim s As IO.Stream = req.GetRequestStream
        s.Write(headerbytes, 0, headerbytes.Length)
        Dim filebytes() As Byte = My.Computer.FileSystem.ReadAllBytes(filepath)
        s.Write(filebytes, 0, filebytes.Length)
        s.Write(endboundarybytes, 0, endboundarybytes.Length)
        s.Close()
        Dim khf As StreamReader
        khf = New StreamReader(req.GetResponse.GetResponseStream)
        Return khf.ReadToEnd

    End Function
    Public Shared Function GetMimeType(ByVal file As String) As String
        Dim mime As String = Nothing
        Dim MaxContent As Integer = CInt(New FileInfo(file).Length)
        If MaxContent > 4096 Then
            MaxContent = 4096
        End If

        Dim fs As New FileStream(file, FileMode.Open)

        Dim buf(MaxContent) As Byte
        fs.Read(buf, 0, MaxContent)
        fs.Close()
        Dim result As Integer = FindMimeFromData(IntPtr.Zero, file, buf, MaxContent, Nothing, 0, mime, 0)
        Select Case mime
            Case "image/pjpeg"
                mime = "image/jpeg"
            Case "image/x-png"
                mime = "image/png"
        End Select
        Return mime
    End Function

End Class
